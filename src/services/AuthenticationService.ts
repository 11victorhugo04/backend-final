import { ApiMlabClient } from "../clients/ApiMlabClient";
import { User } from "../models/User";
import { ApiAwsCognitoClient } from "../clients/ApiAwsCognitoClient";

export class AuthenticationService {

    private apiAwsCognitoClient: ApiAwsCognitoClient;

    constructor() {
        this.apiAwsCognitoClient = new ApiAwsCognitoClient();
    }

    async login(user: User): Promise<any> {
        return await this.apiAwsCognitoClient.loginUser(user);
    }

    async logout(user: User): Promise<any> {
        return await this.apiAwsCognitoClient.logoutUser(user);
    }

    async registerUser(user: User): Promise<any> {
        return await this.apiAwsCognitoClient.registerUser(user);
    }

    async confirmUser(user: User): Promise<any> {
        return await this.apiAwsCognitoClient.confirmUser(user.confirmCode, user);
    }

    async changePassword(user: User): Promise<any> {
        return await this.apiAwsCognitoClient.changePassword(user);
    }

    async forgotPassword(user: User): Promise<any> {
        return await this.apiAwsCognitoClient.forgotPassword(user);
    }

    async confirmPassword(user: User): Promise<any> {
        return await this.apiAwsCognitoClient.confirmPassword(user);
    }
}