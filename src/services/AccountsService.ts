import { ApiMlabClient } from "../clients/ApiMlabClient";
import { Account } from '../models/Account';

export class AccountsService {

    private apiMlabClient: ApiMlabClient;

    constructor() {
        this.apiMlabClient = new ApiMlabClient();
    }

    async getAllAccounts(limit: number, skip: number, user: string): Promise<any> {
        return await this.apiMlabClient.getAllAccounts(limit, skip, user);
    }

    async getAccount(accountNumber: string, user: string): Promise<any> {
        return await this.apiMlabClient.getAccount(accountNumber, user);
    }

    async createAccount(account: Account, user: string): Promise<any> {
        return await this.apiMlabClient.createAccount(account, user);
    }

    async updateAccount(accountNumber: string, account: Account, user: string): Promise<any> {
        return await this.apiMlabClient.updateAccount(accountNumber, account, user);
    }

    async deleteAccount(accountNumber: string, user: string): Promise<any> {
        return await this.apiMlabClient.deleteAccount(accountNumber, user);
    }
}