import { ApiMlabClient } from "../clients/ApiMlabClient";
import { Account } from '../models/Account';
import { Movement } from "models/Movement";

export class MovementsService {

    private apiMlabClient: ApiMlabClient;

    constructor() {
        this.apiMlabClient = new ApiMlabClient();
    }

    async getAllMovements(limit: number, skip: number, user: string): Promise<any> {
        return await this.apiMlabClient.getAllMovements(limit, skip, user);
    }

    async getMovement(movementNumber: string, user: string): Promise<any> {
        return await this.apiMlabClient.getMovement(movementNumber, user);
    }

    async createMovement(movement: Movement, user: string): Promise<any> {
        return await this.apiMlabClient.createMovement(movement, user);
    }

    async updateMovement(movementNumber: string, movement: Movement, user: string): Promise<any> {
        return await this.apiMlabClient.updateMovement(movementNumber, movement, user);
    }

    async deleteMovement(movementNumber: string, user: string): Promise<any> {
        return await this.apiMlabClient.deleteMovement(movementNumber, user);
    }
}