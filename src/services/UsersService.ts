import { ApiAwsCognitoClient } from "../clients/ApiAwsCognitoClient";
import { User } from "../models/User";

export class UsersService {

    private apiAwsCognitoClient: ApiAwsCognitoClient;

    constructor() {
        this.apiAwsCognitoClient = new ApiAwsCognitoClient();
    }

    async getUser(user: User): Promise<any> {
        return await this.apiAwsCognitoClient.getUser(user);
    }
}