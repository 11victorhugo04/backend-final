import { ApiMlabClient } from "../clients/ApiMlabClient";
import { User } from "../models/User";

export class LocationsService {

    private apiMlabClient: ApiMlabClient;

    constructor() {
        this.apiMlabClient = new ApiMlabClient();
    }

    async getAllOffices(limit: number): Promise<any> {
        return await this.apiMlabClient.getAllOffices(limit);
    }

    async getAllAgencies(limit: number): Promise<any> {
        return await this.apiMlabClient.getAllAgencies(limit);
    }

    async getAllAtms(limit: number): Promise<any> {
        return await this.apiMlabClient.getAllAtms(limit);
    }
}