import * as winston from 'winston';
import * as DailyRotateFile from 'winston-daily-rotate-file';
import * as config from 'config';

const transportFile = new DailyRotateFile({
    dirname: config.get('logging.dirname'),
    filename: config.get('logging.filename'),
    datePattern: 'YYYY-MM-DD',
    zippedArchive: true,
    maxSize: '20m',
    maxFiles: 10
});

const transportConsole = new winston.transports.Console({
    type: 'verbose',
    colorize: true,
    prettyPrint: true,
    handleExceptions: true,
    humanReadableUnhandledException: true
});

export const LOGGER: winston.Logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.printf(
            info => `${info.timestamp} ${info.level}: ${info.message}`
        )
    )
});

const env = 'development';

if (env === 'development') {
    LOGGER.add(transportFile);
    LOGGER.add(transportConsole);
}

process.on('unhandledRejection', function (reason, p) {
    LOGGER.warn('system level exceptions at, Possibly Unhandled Rejection at: Promise ', p, ' reason: ', reason);
});