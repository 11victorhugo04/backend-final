import * as config from 'config';
import { ExpressConfig } from './Express';
import { LOGGER } from './AppLogging';

export class Application {

    server: any;
    express: ExpressConfig;

    constructor() {
        global['fetch'] = require('node-fetch');

        this.express = new ExpressConfig();

        const port = config.get('express.port');
        const debug = config.get('express.debug');

        this.server = this.express.app.listen(port, () => {
            LOGGER.info(`
            --------------------------------------------------
            Server Started! Express: http://localhost:${port}
            Health : http://localhost:${port}/ping
            Debugger: http:/${this.server.address()}:${port}/?ws=${this.server.address()}:${port}&port=${debug}
            ------------------------------------------------------
            `)
        });
    }

}