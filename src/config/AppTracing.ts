import * as opentracing from "opentracing";

const initTracer = require('jaeger-client').initTracer;

const configOpenTracing = {
    serviceName: 'backend-final',
};

const optionsOpenTracing = {
    contextKey: 'app-trace-id',
    tags: {
        'backend-final.version': '1.0.0',
    }
};

export const TRACER: opentracing.Tracer = initTracer(configOpenTracing, optionsOpenTracing);