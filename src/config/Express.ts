import * as express from 'express';
import * as cors from 'cors';
import * as helmet from 'helmet';
import * as cookieParser from 'cookie-parser';
import * as compression from 'compression';
import * as responseTime from 'response-time';
import * as bodyParser from 'body-parser';
import * as health from 'express-ping';
import * as path from 'path';
import { Request, Response } from 'express';
import { Container } from "typedi";
import { useExpressServer, useContainer } from 'routing-controllers';

import { LoggingBeforeMiddleware } from '../middlewares/LoggingBeforeMiddleware';
import { LoggingAfterMiddleware } from '../middlewares/LoggingAfterMiddleware';
import { ErrorHandlerMiddleware } from '../middlewares/ErrorHandlerMiddleware';

export class ExpressConfig {

    app: express.Express;

    constructor() {
        this.app = express();
        this.app.use(cors());
        this.app.use(helmet());
        this.app.use(cookieParser());
        this.app.use(compression());
        this.app.use(responseTime());
        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());
        this.app.use(health.ping());
        this.app.use(this.clientErrorHandler);
        this.app.options('*', cors());
        this.setUpControllers();
    }

    setUpControllers() {
        const controllersPath = path.resolve('dist', 'controllers');
        const middlewaresPath = path.resolve('dist', 'middlewares');

        useContainer(Container);
        useExpressServer(this.app, {
            routePrefix: "/api",
            controllers: [controllersPath + "/*.js"],
            middlewares: [LoggingBeforeMiddleware, LoggingAfterMiddleware, ErrorHandlerMiddleware],
            defaultErrorHandler: false,
            validation: true,
            cors: true
        });
    }

    clientErrorHandler(err: any, req: Request, res: Response, next: Function): void {
        if (err.hasOwnProperty('thrown')) {
            res.status(err["status"]).send({ error: err.message });
        }
    }

}