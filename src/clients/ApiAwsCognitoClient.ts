import * as config from 'config';
import * as AwsCognito from "amazon-cognito-identity-js";
import { User } from "../models/User";
import { LOGGER } from '../config/AppLogging';

export class ApiAwsCognitoClient {

    cognitoUserPool: AwsCognito.CognitoUserPool;

    constructor() {
        this.cognitoUserPool = new AwsCognito.CognitoUserPool({
            UserPoolId: config.get('awsCognito.userPoolId'),
            ClientId: config.get('awsCognito.clientId'),
            Storage: new AwsCognito.CookieStorage({
                domain: 'localhost',
                path: '/',
                expires: 365,
                secure: false
            })
        });
    }

    async loginUser(user: User): Promise<any> {
        let cognitoUser: AwsCognito.CognitoUser = new AwsCognito.CognitoUser({
            Pool: this.cognitoUserPool,
            Username: user.email
        });

        let authenticationDetails: AwsCognito.AuthenticationDetails = new AwsCognito.AuthenticationDetails({
            Username: user.email,
            Password: user.password
        });

        return new Promise<any>((resolve, reject) => {
            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: function (result) {
                    let idToken = result.getIdToken().getJwtToken();
                    let accessToken = result.getAccessToken().getJwtToken();
                    let refreshToken = result.getRefreshToken().getToken();
                    resolve({
                        idToken: idToken,
                        accessToken: accessToken,
                        refreshToken: refreshToken
                    });
                },
                onFailure: function (err) {
                    reject(err);
                },
            });
        });
    }

    async logoutUser(user: User): Promise<any> {
        let cognitoUser: AwsCognito.CognitoUser = new AwsCognito.CognitoUser({
            Pool: this.cognitoUserPool,
            Username: user.email
        });

        return new Promise<any>((resolve, reject) => {
            cognitoUser.signOut();

            resolve({});
        });
    }

    async registerUser(user: User): Promise<any> {
        let attributeList: AwsCognito.CognitoUserAttribute[] = [];

        let attributeFirstName = new AwsCognito.CognitoUserAttribute({
            Name: 'custom:first_name',
            Value: user.firstName
        });
        let attributeLastName = new AwsCognito.CognitoUserAttribute({
            Name: 'custom:last_name',
            Value: user.lastName
        });
        let attributeEmail = new AwsCognito.CognitoUserAttribute({
            Name: 'email',
            Value: user.email
        });
        let attributePhoneNumber = new AwsCognito.CognitoUserAttribute({
            Name: 'phone_number',
            Value: user.phoneNumber
        });

        attributeList.push(attributeFirstName);
        attributeList.push(attributeLastName);
        attributeList.push(attributeEmail);
        attributeList.push(attributePhoneNumber);

        return new Promise<any>((resolve, reject) => {
            this.cognitoUserPool.signUp(user.email, user.password, attributeList, attributeList, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    async getUser(user: User): Promise<any> {
        let cognitoUser: AwsCognito.CognitoUser = new AwsCognito.CognitoUser({
            Pool: this.cognitoUserPool,
            Username: user.email
        });

        cognitoUser.setSignInUserSession(new AwsCognito.CognitoUserSession({
            IdToken: new AwsCognito.CognitoIdToken({ IdToken: user.idToken }),
            AccessToken: new AwsCognito.CognitoAccessToken({ AccessToken: user.accessToken }),
            RefreshToken: new AwsCognito.CognitoRefreshToken({ RefreshToken: user.refreshToken }),
        }));

        return new Promise<any>((resolve, reject) => {
            cognitoUser.getSession(function (err, session) {
                if (err) {
                    reject(err);
                } else {
                    cognitoUser.getUserAttributes(function (err, attributes) {
                        if (err) {
                            reject(err);
                        } else {
                            let user: User = new User();

                            attributes.forEach(function (attribute) {
                                switch (attribute.getName()) {
                                    case "custom:first_name":
                                        user.firstName = attribute.getValue();
                                        break;
                                    case "custom:last_name":
                                        user.lastName = attribute.getValue();
                                        break;
                                    case "phone_number":
                                        user.phoneNumber = attribute.getValue();
                                        break;
                                    case "email":
                                        user.email = attribute.getValue();
                                        break;
                                    default:
                                        break;
                                }
                            });

                            user.completeName = user.firstName + ' ' + user.lastName;

                            resolve(user);
                        }
                    });
                }
            });
        });
    }

    async confirmUser(confirmCode: string, user: User): Promise<any> {
        let cognitoUser: AwsCognito.CognitoUser = new AwsCognito.CognitoUser({
            Pool: this.cognitoUserPool,
            Username: user.email
        });

        return new Promise<any>((resolve, reject) => {
            cognitoUser.confirmRegistration(confirmCode, true, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    async changePassword(user: User): Promise<any> {
        let cognitoUser: AwsCognito.CognitoUser = new AwsCognito.CognitoUser({
            Pool: this.cognitoUserPool,
            Username: user.email
        });

        return new Promise<any>((resolve, reject) => {
            cognitoUser.changePassword(user.oldPassword, user.newPassword, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
    }

    async forgotPassword(user: User): Promise<any> {
        let cognitoUser: AwsCognito.CognitoUser = new AwsCognito.CognitoUser({
            Pool: this.cognitoUserPool,
            Username: user.email
        });

        return new Promise<any>((resolve, reject) => {
            cognitoUser.forgotPassword({
                onSuccess: function (result) {
                    resolve(result);
                },
                onFailure: function (err) {
                    reject(err);
                }
            });
        });
    }

    async confirmPassword(user: User): Promise<any> {
        let cognitoUser: AwsCognito.CognitoUser = new AwsCognito.CognitoUser({
            Pool: this.cognitoUserPool,
            Username: user.email
        });

        return new Promise<any>((resolve, reject) => {
            cognitoUser.confirmPassword(user.confirmCode, user.newPassword, {
                onSuccess: () => {
                    resolve({});
                },
                onFailure: (err) => {
                    reject(err);
                },
            });
        });
    }
}