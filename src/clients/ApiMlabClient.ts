import * as config from 'config';
import * as requestPromise from "request-promise";
import { User } from "../models/User";
import { Account } from '../models/Account';
import { LOGGER } from "../config/AppLogging";
import { Movement } from 'models/Movement';

export class ApiMlabClient {

    constructor() {
    }

    async getAllUsers(limit: number): Promise<any> {
        return await requestPromise.get('user', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                f: '{"_id":0}',
                sk: 20,
                l: limit
            },
            json: true,
            timeout: 4000
        });
    }

    async getUser(userId: number): Promise<any> {
        return await requestPromise.get('user', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                f: '{"_id":0}',
                q: '{"userid":' + userId + '}'
            }
        });
    }

    async createUser(user: User): Promise<any> {
        return await requestPromise.post('user', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey')
            },
            json: true,
            body: user
        });
    }

    async updateUser(userId: number, user: User): Promise<any> {
        return await requestPromise.put('user', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"userid":' + userId + '}'
            },
            json: true,
            body: JSON.stringify({ "$set": user }),
        });
    }

    async deleteUser(userId: number): Promise<any> {
        return await requestPromise.put('user', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"userid":' + userId + '}'
            },
            json: true,
            body: {}
        });
    }

    async getAllAccounts(limit: number, skip: number, user: string): Promise<any> {
        return await requestPromise.get('account', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"user":' + user + '}',
                sk: skip,
                l: limit
            },
            json: true,
            timeout: 5000
        });
    }

    async getAccount(accountNumber: string, user: string): Promise<any> {
        return await requestPromise.get('account', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                f: '{"_id":0}',
                q: '{"accountNumber":' + accountNumber + ',"user":' + user + '}'
            },
            timeout: 5000
        });
    }

    async createAccount(account: Account, user: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            requestPromise.get('account', {
                baseUrl: config.get('apiMlab.url'),
                qs: {
                    apiKey: config.get('apiMlab.apiKey'),
                    c: true
                },
                json: true,
                timeout: 5000
            }).then(function (response) {
                account.accountName = 'Cuenta ' + (response + 1);
                account.accountNumber = (response + 1).toString().padStart(15, '0');
                account.currentBalance = account.openingAmount;
                account.availableBalance = account.openingAmount;
                account.user = user;

                requestPromise.post('account', {
                    baseUrl: config.get('apiMlab.url'),
                    qs: {
                        apiKey: config.get('apiMlab.apiKey')
                    },
                    json: true,
                    body: account,
                    timeout: 5000
                }).then(function (response) {
                    resolve(response);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
    }

    async updateAccount(accountNumber: string, account: Account, user: string): Promise<any> {
        account.user = user;

        return await requestPromise.put('account', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"accountNumber":' + accountNumber + ',"user":' + user + '}'
            },
            json: true,
            body: JSON.stringify({ "$set": account }),
            timeout: 5000
        });
    }

    async deleteAccount(accountNumber: string, user: string): Promise<any> {
        return await requestPromise.put('account', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"accountNumber":' + accountNumber + ',"user":' + user + '}'
            },
            json: true,
            body: {},
            timeout: 5000
        });
    }

    async getAllMovements(limit: number, skip: number, user: string): Promise<any> {
        return await requestPromise.get('movement', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"user":' + user + '}',
                sk: skip,
                l: limit
            },
            json: true,
            timeout: 5000
        });
    }

    async getMovement(movementNumber: string, user: string): Promise<any> {
        return await requestPromise.get('movement', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                f: '{"_id":0}',
                q: '{"movementNumber":' + movementNumber + ',"user":' + user + '}'
            },
            timeout: 5000
        });
    }

    async createMovement(movement: Movement, user: string): Promise<any> {
        return new Promise<any>((resolve, reject) => {
            requestPromise.get('movement', {
                baseUrl: config.get('apiMlab.url'),
                qs: {
                    apiKey: config.get('apiMlab.apiKey'),
                    c: true
                },
                json: true,
                timeout: 5000
            }).then(function (response) {
                movement.movementNumber = (response + 1).toString().padStart(15, '0');
                movement.movementDate = new Date().getTime();
                movement.description = 'Movimiento ' + (response + 1);
                movement.user = user;

                requestPromise.post('movement', {
                    baseUrl: config.get('apiMlab.url'),
                    qs: {
                        apiKey: config.get('apiMlab.apiKey')
                    },
                    json: true,
                    body: movement,
                    timeout: 5000
                }).then(function (response) {
                    resolve(response);
                }).catch(function (err) {
                    reject(err);
                });
            }).catch(function (err) {
                reject(err);
            });
        });
    }

    async updateMovement(movementNumber: string, movement: Movement, user: string): Promise<any> {
        movement.user = user;

        return await requestPromise.put('movement', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"movementNumber":' + movementNumber + ',"user":' + user + '}'
            },
            json: true,
            body: JSON.stringify({ "$set": movement }),
            timeout: 5000
        });
    }

    async deleteMovement(movementNumber: string, user: string): Promise<any> {
        return await requestPromise.put('movement', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"movementNumber":' + movementNumber + ',"user":' + user + '}'
            },
            json: true,
            body: {}
        });
    }

    async getAllOffices(limit: number): Promise<any> {
        return await requestPromise.get('location', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"type":"OFICINA"}'
            },
            json: true,
            timeout: 5000
        });
    }

    async getAllAgencies(limit: number): Promise<any> {
        return await requestPromise.get('location', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"type":"AGENTE"}'
            },
            json: true,
            timeout: 5000
        });
    }

    async getAllAtms(limit: number): Promise<any> {
        return await requestPromise.get('location', {
            baseUrl: config.get('apiMlab.url'),
            qs: {
                apiKey: config.get('apiMlab.apiKey'),
                q: '{"type":"ATM"}'
            },
            json: true,
            timeout: 5000
        });
    }
}