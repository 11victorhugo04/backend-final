import { JsonController, Get, Post, Body, HttpCode } from 'routing-controllers';
import { AuthenticationService } from '../services/AuthenticationService';
import { AppConstants } from '../utils/AppConstants';
import { User } from '../models/User';

@JsonController(AppConstants.PATH_AUTHENTICATION)
export class AuthenticationController {

   private authenticationService: AuthenticationService;

   constructor() {
      this.authenticationService = new AuthenticationService();
   }

   @Post('/login')
   @HttpCode(200)
   async login(@Body({ validate: false }) user: User): Promise<any> {
      return await this.authenticationService.login(user);
   }

   @Post('/logout')
   @HttpCode(200)
   async logout(@Body({ validate: false }) user: User): Promise<any> {
      return await this.authenticationService.logout(user);
   }

   @Post('/user/register')
   @HttpCode(200)
   async registerUser(@Body({ validate: false }) user: User): Promise<any> {
      return await this.authenticationService.registerUser(user);
   }

   @Post('/user/confirm')
   @HttpCode(200)
   async confirmUser(@Body({ validate: false }) user: User): Promise<any> {
      return await this.authenticationService.confirmUser(user);
   }

   @Post('/password/change')
   @HttpCode(200)
   async changePassword(@Body({ validate: false }) user: User): Promise<any> {
      return await this.authenticationService.changePassword(user);
   }

   @Post('/password/forgot')
   @HttpCode(200)
   async forgotPassword(@Body({ validate: false }) user: User): Promise<any> {
      return await this.authenticationService.forgotPassword(user);
   }

   @Post('/password/confirm')
   @HttpCode(200)
   async confirmPassword(@Body({ validate: false }) user: User): Promise<any> {
      return await this.authenticationService.confirmPassword(user);
   }
}