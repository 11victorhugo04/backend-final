import { JsonController, Get, Post, Put, Param, Body, HttpCode, OnUndefined, Delete, QueryParam, HeaderParam } from 'routing-controllers';
import { AccountsService } from '../services/AccountsService';
import { AppConstants } from '../utils/AppConstants';
import { Account } from '../models/Account';
import { MovementsService } from '../services/MovementsService';
import { Movement } from '../models/Movement';

@JsonController(AppConstants.PATH_MOVEMENTS)
export class MovementsController {

    private movementService: MovementsService;

    constructor() {
        this.movementService = new MovementsService();
    }

    @Get('/')
    @OnUndefined(404)
    async getAll(@HeaderParam("sub") user: string,
        @QueryParam("limit") limit: number, @QueryParam("skip") skip: number): Promise<any> {
        return await this.movementService.getAllMovements(limit, skip, user);
    }

    @Get('/:movementNumber')
    @OnUndefined(404)
    async get(@HeaderParam("sub") user: string,
        @Param("movementNumber") movementNumber: string): Promise<any> {
        return await this.movementService.getMovement(movementNumber, user);
    }

    @Post('/')
    @HttpCode(201)
    async create(@HeaderParam("sub") user: string,
        @Body({ validate: true }) movement: Movement): Promise<any> {
        return await this.movementService.createMovement(movement, user);
    }

    @Put('/:movementNumber')
    @HttpCode(200)
    async update(@HeaderParam("sub") user: string,
        @Param("movementNumber") movementNumber: string,
        @Body({ validate: true }) movement: Movement): Promise<any> {
        return await this.movementService.updateMovement(movementNumber, movement, user);
    }

    @Delete('/:movementNumber')
    @HttpCode(204)
    async delete(@HeaderParam("sub") user: string,
        @Param("movementNumber") movementNumber: string): Promise<any> {
        return await this.movementService.deleteMovement(movementNumber, user);
    }
}