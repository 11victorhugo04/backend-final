import { JsonController, Get, Post, Put, Param, Body, HttpCode, OnUndefined, Delete, QueryParam, HeaderParam } from 'routing-controllers';
import { AccountsService } from '../services/AccountsService';
import { AppConstants } from '../utils/AppConstants';
import { Account } from '../models/Account';

@JsonController(AppConstants.PATH_ACCOUNTS)
export class AccountsController {

    private accountsService: AccountsService;

    constructor() {
        this.accountsService = new AccountsService();
    }

    @Get('/')
    @OnUndefined(404)
    async getAll(@HeaderParam("sub") user: string,
        @QueryParam("limit") limit: number,
        @QueryParam("skip") skip: number): Promise<any> {
        return await this.accountsService.getAllAccounts(limit, skip, user);
    }

    @Get('/:accountNumber')
    @OnUndefined(404)
    async get(@HeaderParam("sub") user: string,
        @Param("accountNumber") accountNumber: string): Promise<any> {
        return await this.accountsService.getAccount(accountNumber, user);
    }

    @Post('/')
    @HttpCode(201)
    async create(@HeaderParam("sub") user: string,
        @Body({ validate: true }) account: Account): Promise<any> {
        return await this.accountsService.createAccount(account, user);
    }

    @Put('/:accountNumber')
    @HttpCode(200)
    async update(@HeaderParam("sub") user: string,
        @Param("accountNumber") accountNumber: string,
        @Body({ validate: true }) account: Account): Promise<any> {
        return await this.accountsService.updateAccount(accountNumber, account, user);
    }

    @Delete('/:accountNumber')
    @HttpCode(204)
    async delete(@HeaderParam("sub") user: string,
        @Param("accountNumber") accountNumber: string): Promise<any> {
        return await this.accountsService.getAccount(accountNumber, user);
    }
}