import { JsonController, Get, Post, Put, Param, Body, HttpCode, OnUndefined, Delete, QueryParam, HeaderParam } from 'routing-controllers';
import { UsersService } from '../services/UsersService';
import { AppConstants } from '../utils/AppConstants';
import { User } from '../models/User';

@JsonController(AppConstants.PATH_USERS)
export class UsersController {

    private usersService: UsersService;

    constructor() {
        this.usersService = new UsersService();
    }

    @Get('/:username')
    @OnUndefined(404)
    async get(@Param("username") username: string,
        @HeaderParam("idtoken") idToken: string,
        @HeaderParam("accesstoken") accessToken: string,
        @HeaderParam("refreshtoken") refreshToken: string): Promise<any> {
        let user: User = new User();
        user.email = username;
        user.idToken = idToken;
        user.accessToken = accessToken;
        user.refreshToken = refreshToken;
        return await this.usersService.getUser(user);
    }
}