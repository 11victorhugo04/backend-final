import { JsonController, Get, Post, Put, Param, Body, HttpCode, OnUndefined, Delete, QueryParam } from 'routing-controllers';
import { LocationsService } from '../services/LocationsService';
import { AppConstants } from '../utils/AppConstants';

@JsonController(AppConstants.PATH_LOCATIONS)
export class LocationsController {

    private locationsService: LocationsService;

    constructor() {
        this.locationsService = new LocationsService();
    }

    @Get('/offices')
    @OnUndefined(404)
    async getAllOffices(@QueryParam("limit") limit: number): Promise<any> {
        return await this.locationsService.getAllOffices(limit);
    }

    @Get('/agencies')
    @OnUndefined(404)
    async getAllAgencies(@QueryParam("limit") limit: number): Promise<any> {
        return await this.locationsService.getAllAgencies(limit);
    }

    @Get('/atms')
    @OnUndefined(404)
    async getAllAtms(@QueryParam("limit") limit: number): Promise<any> {
        return await this.locationsService.getAllAtms(limit);
    }
}