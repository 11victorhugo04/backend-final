export class DebitCard {

    typeDebitCard: string;

    typeCategory: string;
    
    typeCurrency: string;

    openingDate: Date;
    
    associatedAccount: string;

    deliveryAddress: string;

    telephoneContact: string;

    confirmTermsConditions: boolean;

    nameDebitCard: string;

    debitCardNumber: string;

    currentBalance: Number;

    availableBalance: Number;
}