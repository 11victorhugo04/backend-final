export class CreditCard {

    typeCreditCard: string;

    typeCategory: string;

    typeCurrency: string;

    openingDate: Date;
    
    creditLimit: Number;

    paymentDate: Date;

    paymentAccount: string;

    deliveryAddress: string;

    telephoneContact: string;

    confirmTermsConditions: boolean;

    nameCreditCard: string;

    creditCardNumber: string;

    currentBalance: Number;

    availableBalance: Number;
}