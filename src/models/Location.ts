export class Location {

    type: string;

    name: string;

    hours: string;

    latitude: number;

    longitude: number;
}