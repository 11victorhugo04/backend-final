export class Movement {

    typeMovement: string;

    typeTransaction: string;

    typeCurrency: string;

    movementNumber: string;

    movementDate: number;

    description: string;

    accountCharge: string;

    accountSubscription: string;

    amount: Number;

    emailNotification: string;

    smsNotification: string;

    user: string;
}