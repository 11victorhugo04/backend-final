export class Account {

    typeAccount: string;

    typeCategory: string;

    typeCurrency: string;

    openingDate: Date;

    openingAmount: Number;

    ocupationCompanyName: string;
    
    ocupationName: string;

    ocupationAverageSalary: Number;

    phoneNumber: string;
    
    confirmationCode: string;
    
    confirmIdentityData: boolean;

    confirmOccupationData: boolean;

    confirmTermsConditions: boolean;

    accountName: string;

    accountNumber: string;

    interbankAccountNumber: string;

    currentBalance: Number;

    availableBalance: Number;

    user: string;
}