export class Audit {

    id: string;

    creationDate: Date;

    creationUser: string;

    modificationDate: Date;

    modificationUser: string;

}