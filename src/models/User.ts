import { IsEmail, MinLength } from "class-validator";

export class User {

    id: string;

    @IsEmail({})
    email: string;

    @MinLength(8)
    password: string;

    documentType: string;

    documentNumber: string;

    firstName: string;

    lastName: string;

    completeName: string;
    
    phoneNumber: string;

    homeAddress: string;

    workAddress: string;

    confirmCode: string;

    oldPassword: string;
    
    newPassword: string;

    idToken: string;

    accessToken: string;

    refreshToken: string;
}