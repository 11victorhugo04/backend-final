import { Middleware, ExpressMiddlewareInterface } from "routing-controllers";
import * as opentracing from "opentracing";
import * as url from "url";
import { TRACER } from "../config/AppTracing";
import { LOGGER } from "../config/AppLogging";

@Middleware({ type: "after", priority: 2 })
export class LoggingAfterMiddleware implements ExpressMiddlewareInterface {

    use(request: any, response: any, next: (err: any) => any): void {
        LOGGER.info('LoggingAfterMiddleware');

        Object.keys(response._headers).forEach(key => LOGGER.info(key + ' ' + response._headers[key]));

        next(null);
    }
}