import { Middleware, ExpressMiddlewareInterface } from "routing-controllers";
import * as opentracing from "opentracing";
import * as url from "url";
import { TRACER } from "../config/AppTracing";
import { LOGGER } from "../config/AppLogging";

@Middleware({ type: "before", priority: 1 })
export class LoggingBeforeMiddleware implements ExpressMiddlewareInterface {

    use(request: any, response: any, next: (err: any) => any): void {
        LOGGER.info('APP START REQUEST: ');

        const context = TRACER.extract(opentracing.FORMAT_HTTP_HEADERS, request.headers);
        const pathname = url.parse(request.url).pathname;
        const operationName = (request.route && request.route.path) || pathname;

        const span = TRACER.startSpan(pathname, { childOf: context });
        span.setOperationName(operationName);
        span.setTag("http.method", request.method);
        span.setTag("http.url", request.url);

        const responseHeaders = {};
        TRACER.inject(span.context(), opentracing.FORMAT_HTTP_HEADERS, responseHeaders);
        Object.keys(responseHeaders).forEach(key => response.setHeader(key, responseHeaders[key]));
        Object.assign(request, { span });

        Object.keys(response._headers).forEach(key => LOGGER.info(key + ' ' + response._headers[key]));

        const finishSpan = () => {
            span.setTag("http.status_code", response.statusCode);
            if (response.statusCode != 200) {
                span.setTag("error", true);
                span.setTag("priority", 1);
            }
            span.finish();
        };
        response.on('close', finishSpan);
        response.on('finish', finishSpan);

        next(null);
    }
}