import { Middleware, ExpressErrorMiddlewareInterface } from "routing-controllers";
import { LOGGER } from "../config/AppLogging";

@Middleware({ type: "after", priority: 1 })
export class ErrorHandlerMiddleware implements ExpressErrorMiddlewareInterface {

    error(error: any, request: any, response: any, next: (err: any) => any) {
        LOGGER.info('ErrorHandlerMiddleware');
        
        error.statusCode = error.statusCode ? error.statusCode : 503;
        error.message = error.message ? error.message : 'Servicio no disponible';

        response.status(error.statusCode);
        response.json(error);
    }

}