export namespace AppConstants {
    export const PATH_AUTHENTICATION = '/authentication';
    export const PATH_USERS = '/users';
    export const PATH_ACCOUNTS = '/accounts';
    export const PATH_MOVEMENTS = '/movements';
    export const PATH_LOCATIONS = '/locations';
}